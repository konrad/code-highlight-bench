package main

import (
	"bufio"
	"bytes"
	"fmt"
	"github.com/alecthomas/chroma/quick"
	"github.com/sourcegraph/syntaxhighlight"
	"time"
)

func main() {
	start := time.Now()

	var b bytes.Buffer
	writer := bufio.NewWriter(&b)

	quick.Highlight(writer, file, "go", "html", "monokai")

	fmt.Printf("chroma quick highlight took %v\n", time.Since(start))

	start = time.Now()
	syntaxhighlight.AsHTML([]byte(file))
	fmt.Printf("sourcegraph syntaxhighlight took %v\n", time.Since(start))
}
