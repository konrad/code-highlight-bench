module git.kolaente.de/konrad/go-chroma-bench

go 1.12

require (
	github.com/alecthomas/chroma v0.6.3
	github.com/alecthomas/kong v0.1.16 // indirect
	github.com/alecthomas/repr v0.0.0-20181024024818-d37bc2a10ba1 // indirect
	github.com/mattn/go-colorable v0.1.1 // indirect
	github.com/mattn/go-isatty v0.0.7 // indirect
	github.com/sourcegraph/annotate v0.0.0-20160123013949-f4cad6c6324d // indirect
	github.com/sourcegraph/syntaxhighlight v0.0.0-20170531221838-bd320f5d308e
	github.com/stretchr/objx v0.1.1 // indirect
	github.com/stretchr/testify v1.3.0 // indirect
	golang.org/x/sys v0.0.0-20190405154228-4b34438f7a67 // indirect
)
